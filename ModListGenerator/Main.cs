﻿using ModListGenerator.Business.Services;
using ModListGenerator.Business.Services.exporters;
using ModListGenerator.Business.Services.importers;
using ModListGenerator.Entities.Models;
using System.Security;
using System.Windows.Forms;

namespace ModListGenerator
{
    public partial class Main : Form
    {
        private readonly OpenFileDialog? openFileDialog;
        private readonly SaveFileDialog? saveFileDialog;
        private ImportService? importService;
        private ExportService? exportService;
        private HttpService? httpService;
        private readonly HttpConfiguration httpConfiguration;
        private ModList mods;

        private static readonly string skyrimLink = "https://www.nexusmods.com/skyrim/mods/";
        private static readonly string skyrimApiLink = "https://api.nexusmods.com/v1/games/skyrim/mods/";
        private static readonly string skyrimSELink = "https://www.nexusmods.com/skyrimspecialedition/mods/";
        private static readonly string skyrimSEApiLink = "https://api.nexusmods.com/v1/games/skyrimspecialedition/mods/";

        public Main()
        {
            InitializeComponent();
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            openFileDialog = new OpenFileDialog()
            {
                FileName = "Sélectionnez un fichier csv ou xlsx",
                Filter = "Fichiers csv ou xlsx (*csv, *xlsx)|*.csv;*xlsx",
                Title = "Ouvrez un fichier csv ou xlsx"
            };

            saveFileDialog = new SaveFileDialog()
            {
                Filter = "Fichiers csv ou xlsx (*csv, *xlsx)|*.csv;*xlsx",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            mods = new ModList();
            httpConfiguration = new HttpConfiguration { UpdateNexusMods = true };
        }

        private void ImportButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog?.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string? filePath = openFileDialog.FileName;
                    if (filePath.Contains("xlsx"))
                    {
                        importService = new XLSXImporter(filePath);
                    }
                    else if (filePath.Contains("csv"))
                    {
                        importService = new CSVImporter(filePath);
                    }
                    if (importService != null)
                    {
                        mods = importService.Import();
                        importButton.Visible = false;
                        exportButton.Visible = true;
                        updateOption.Visible = true;
                        groupBox.Visible = false;
                    }
                }
                catch (SecurityException ex)
                {
                    _ = MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }

        private void ExportButton_Click(object sender, EventArgs e)
        {
            httpConfiguration.UpdateNexusMods = updateOption.Checked;
            if (radioButton.Checked)
            {
                httpConfiguration.Link = skyrimLink;
                httpConfiguration.ApiLink = skyrimApiLink;
            }
            else
            {
                httpConfiguration.Link = skyrimSELink;
                httpConfiguration.ApiLink = skyrimSEApiLink;
            }
            httpService = new HttpService(httpConfiguration);
            if (saveFileDialog?.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    InitProgressBar();
                    mods = httpService.RetrieveDataAsync(mods, progressBar);
                    string? filePath = saveFileDialog.FileName;
                    if (filePath.Contains("xlsx"))
                    {
                        exportService = new XLSXExporter(filePath);
                    }
                    else if (filePath.Contains("csv"))
                    {
                        exportService = new CSVExporter(filePath);
                    }
                    if (exportService != null)
                    {
                        exportService.Export(mods);
                        importButton.Visible = true;
                        exportButton.Visible = false;
                        updateOption.Visible = false;
                        groupBox.Visible = true;
                    }
                    progressBar.Visible = false;
                }
                catch (Exception ex)
                {
                    _ = MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }

        private void InitProgressBar()
        {
            progressBar.Visible = true;
            progressBar.Minimum = 1;
            progressBar.Value = 1;
            progressBar.Step = 1;
        }
    }
}