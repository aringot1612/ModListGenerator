﻿
using System.Windows.Controls;

namespace ModListGenerator
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exportButton = new System.Windows.Forms.Button();
            this.importButton = new System.Windows.Forms.Button();
            this.updateOption = new System.Windows.Forms.CheckBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.radioButton = new System.Windows.Forms.RadioButton();
            this.radioButtonSE = new System.Windows.Forms.RadioButton();
            this.groupBox.SuspendLayout();
            this.groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // exportButton
            // 
            this.exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.exportButton.Location = new System.Drawing.Point(192, 96);
            this.exportButton.Name = "exportButton";
            this.exportButton.Size = new System.Drawing.Size(128, 64);
            this.exportButton.TabIndex = 1;
            this.exportButton.Text = "Exporter";
            this.exportButton.UseVisualStyleBackColor = true;
            this.exportButton.Visible = false;
            this.exportButton.Click += new System.EventHandler(this.ExportButton_Click);
            // 
            // importButton
            // 
            this.importButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.importButton.Location = new System.Drawing.Point(192, 96);
            this.importButton.Name = "importButton";
            this.importButton.Size = new System.Drawing.Size(128, 64);
            this.importButton.TabIndex = 2;
            this.importButton.Text = "Importer";
            this.importButton.UseVisualStyleBackColor = true;
            this.importButton.Click += new System.EventHandler(this.ImportButton_Click);
            // 
            // updateOption
            // 
            this.updateOption.AutoSize = true;
            this.updateOption.Location = new System.Drawing.Point(57, 225);
            this.updateOption.Name = "updateOption";
            this.updateOption.Size = new System.Drawing.Size(379, 19);
            this.updateOption.TabIndex = 3;
            this.updateOption.Text = "Cochez cette case pour activer la mise à jour des mods déjà ajoutés";
            this.updateOption.UseVisualStyleBackColor = true;
            this.updateOption.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 12);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(488, 23);
            this.progressBar.TabIndex = 4;
            this.progressBar.Visible = false;
            //
            // groupBox
            //
            this.groupBox.Controls.Add(this.radioButton);
            this.groupBox.Controls.Add(this.radioButtonSE);
            this.groupBox.Location = new System.Drawing.Point(192, 180);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(128, 64);
            this.groupBox.TabIndex = 5;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "Version de skyrim";
            // 
            // radioButton
            // 
            this.radioButton.AutoSize = true;
            this.radioButton.Location = new System.Drawing.Point(6, 20);
            this.radioButton.Name = "radioButton";
            this.radioButton.Size = new System.Drawing.Size(71, 19);
            this.radioButton.TabIndex = 0;
            this.radioButton.TabStop = true;
            this.radioButton.Text = "Normale";
            this.radioButton.UseVisualStyleBackColor = true;
            // 
            // radioButtonSE
            // 
            this.radioButtonSE.AutoSize = true;
            this.radioButtonSE.Location = new System.Drawing.Point(6, 39);
            this.radioButtonSE.Name = "radioButtonSE";
            this.radioButtonSE.Size = new System.Drawing.Size(37, 19);
            this.radioButtonSE.TabIndex = 1;
            this.radioButtonSE.TabStop = true;
            this.radioButtonSE.Text = "SE";
            this.radioButtonSE.UseVisualStyleBackColor = true;
            this.radioButtonSE.Checked = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(512, 256);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.exportButton);
            this.Controls.Add(this.importButton);
            this.Controls.Add(this.updateOption);
            this.Controls.Add(this.progressBar);
            this.Name = "Main";
            this.Text = "Mod List Generator";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button exportButton;
        private System.Windows.Forms.Button importButton;
        private System.Windows.Forms.CheckBox updateOption;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.RadioButton radioButton;
        private System.Windows.Forms.RadioButton radioButtonSE;
    }
}