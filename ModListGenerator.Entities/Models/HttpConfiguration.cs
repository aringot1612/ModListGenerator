﻿namespace ModListGenerator.Entities.Models
{
    public class HttpConfiguration
    {
        public bool UpdateNexusMods { get; set; }
        public string? Link { get; set; }
        public string? ApiLink { get; set; }
    }
}
