﻿namespace ModListGenerator.Entities.Models
{
    public class NexusMod
    {
        public int Mod_id { get; set; }
        public int Category_id { get; set; }
        public string? Name { get; set; }
        public string? Author { get; set; }
        public string? Description { get; set; }
        public string? Summary { get; set; }
        public string? CurrentVersion { get; set; }
        public string? Version { get; set; }
        public int? Endorsement_count { get; set; }
        public string? Created_time { get; set; }
        public string? Updated_time { get; set; }
        public string? Picture_url { get; set; }
        public bool? Contains_adult_content { get; set; }
    }
}