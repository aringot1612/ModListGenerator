﻿namespace ModListGenerator.Entities.Models
{
    public class ModList
    {
        public List<string>? header;
        public List<ModValues>? content;
    }
}