﻿using ModListGenerator.Entities.Models;

namespace ModListGenerator.Core.Interfaces
{
    public interface IExportService
    {
        public void Export(ModList mods);
    }
}
