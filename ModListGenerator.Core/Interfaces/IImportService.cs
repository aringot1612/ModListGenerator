﻿using ModListGenerator.Entities.Models;

namespace ModListGenerator.Core.Interfaces
{
    public interface IImportService
    {
        public ModList Import();
    }
}
