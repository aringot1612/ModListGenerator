# Mod List Generator

## Description :

Ce petit programme permet de compléter un fichier csv ou xlsx comprenant une liste de mods skyrim.

### Formats de fichiers pris en charge :
```
xlsx et csv
```

<br>

### Séparateur csv <b>;</b>

<br>

### Valeurs d'en-tête prises en charge pour l'auto-complétion :

<br>

- id
- category
- name
- author
- description
- summary
- current version
- last version
- endorsement count
- created time
- updated time
- picture url
- contains adult content

<br>

Si le fichier importé contient des en-têtes supplémentaires : ils seront récupérés, avec leurs valeurs, lors de l'exportation.

Pour obtenir des informations sur chaque mod, il faut : 
- soit un en-tête "id" avec des valeurs correctes.
- soit un en-tête "link" avec des liens corrects.

Pour fournir un exemple, voici un tableau excel avant utilisation du programme :

<br>![before](images/before.png)

Une fois ce fichier importé, il suffit de l'exporter pour obtenir : 

<br>![after](images/after.png)

## Auteur :

- [Ringot Arthur](https://gitlab.com/aringot1612)