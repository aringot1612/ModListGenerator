﻿using CsvHelper;
using CsvHelper.Configuration;
using ModListGenerator.Entities.Models;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace ModListGenerator.Business.Services.importers
{
    public class CSVImporter : ImportService
    {
        private readonly StreamReader streamReader;
        private readonly CsvReader csvReader;

        public CSVImporter(string importPath) : base()
        {
            streamReader = File.OpenText(importPath);
            CsvConfiguration? config = new(CultureInfo.InvariantCulture)
            {
                HeaderValidated = null,
                Delimiter = ";",
                HasHeaderRecord = false
            };
            csvReader = new(streamReader, config);
        }

        public override ModList Import()
        {
            ModList modList = new() { content = new List<ModValues>() };
            string value;
            using (streamReader)
            {
                if (csvReader.Read())
                {
                    List<string> result = new();
                    for (int i = 0; csvReader.TryGetField(i, out value); i++)
                    {
                        result.Add(value);
                    }
                    modList.header = result;
                }
                while (csvReader.Read())
                {
                    List<string> result = new();
                    for (int i = 0; csvReader.TryGetField(i, out value); i++)
                    {
                        result.Add(value);
                    }
                    modList.content.Add(new ModValues { values = result });
                }
            }
            return modList;
        }
    }
}
