﻿using ModListGenerator.Entities.Models;
using System.Data;
using OfficeOpenXml;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace ModListGenerator.Business.Services.importers
{
    public class XLSXImporter : ImportService
    {
        private readonly string importPath;

        public XLSXImporter(string importPath) : base()
        {
            this.importPath = importPath;
        }

        public override ModList Import()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ModList modList = new() { content = new List<ModValues>() };
            using ExcelPackage xlPackage = new(new FileInfo(importPath));
            ExcelWorksheet? myWorksheet = xlPackage.Workbook.Worksheets.First(); //select sheet here
            int totalRows = myWorksheet.Dimension.End.Row;
            int totalColumns = myWorksheet.Dimension.End.Column;
            if (myWorksheet != null)
            {
                modList.header = myWorksheet.Cells[1, 1, 1, totalColumns]
                .Select(c => c.Value == null ? "" : c.Value.ToString())
                .ToList();
                StringBuilder? sb = new(); //this is your data
                for (int rowNum = 2; rowNum <= totalRows; rowNum++) //select starting row here
                {
                    List<string> values = new();
                    for (int columnNum = 1; columnNum <= totalColumns; columnNum++) //select starting row here
                    {
                        values.Add(myWorksheet.Cells[rowNum, columnNum].Value == null ? "" : myWorksheet.Cells[rowNum, columnNum].Value.ToString());
                    }
                    modList.content.Add(new ModValues { values = values });
                }
            }
            return modList;
        }
    }
}