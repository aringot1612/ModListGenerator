﻿using ModListGenerator.Core.Interfaces;
using ModListGenerator.Entities.Models;

namespace ModListGenerator.Business.Services.importers
{
    public abstract class ImportService : IImportService
    {
        protected ModList modList;

        public ImportService()
        {
            modList = new ModList();
        }

        public abstract ModList Import();
    }
}
