﻿using ModListGenerator.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModListGenerator.Business.Services
{
    public class HttpService : IDisposable
    {
        private readonly HttpClient client;
        private readonly HttpConfiguration httpConfiguration;
        private readonly ModService modService;
        private readonly string? link;
        private readonly string? apiLink;

        public HttpService(HttpConfiguration httpConfiguration)
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Add("apikey", "cjRKeUl0Z05TaEI0aGdybmhtMkdyNjlOdjRXTjA2R1poKzhNTXdWNUZMR2RFeGRhZlRkLzdZY1VXZm9nM0Qwai0tcDJIZWh5RDNHQVVVNStRdzVWY1ZWdz09--6246eefc83db2db0abd5403ee1436eb38f6a873d");
            this.httpConfiguration = httpConfiguration;
            modService = new();
            link = httpConfiguration.Link;
            apiLink = httpConfiguration.ApiLink;
        }

        public ModList RetrieveDataAsync(ModList modList, ProgressBar progressBar)
        {
            List<string> header = ModService.GetHeader(modList);
            List<Tuple<string, string>> tuples = ModService.GetTuples();
            int idIndex = header.IndexOf("id");
            int linkIndex = header.IndexOf("link");
            if ((idIndex == -1 && linkIndex == -1) || modList.content == null)
            {
                return new ModList();
            }
            progressBar.Maximum = modList.content.Count;
            foreach (ModValues element in modList.content)
            {
                if (httpConfiguration.UpdateNexusMods || !ModService.CheckModList(element.values, header, tuples))
                {
                    string id = string.Empty;
                    if (element != null && element.values != null)
                    {
                        if (idIndex != -1 && int.TryParse(element.values[idIndex], out _))
                        {
                            id = element.values[idIndex];
                        }
                        else if (linkIndex != -1 && element.values[linkIndex].Contains(link))
                        {
                            id = element.values[linkIndex][(element.values[linkIndex].LastIndexOf('/') + 1)..];
                        }
                        if (!string.IsNullOrEmpty(id))
                        {
                            try
                            {
                                ModValues mod = new();
                                HttpResponseMessage res = client.GetAsync(apiLink + id + ".json").Result;
                                if (res.IsSuccessStatusCode)
                                {
                                    Task<NexusMod?>? response = res.Content.ReadFromJsonAsync<NexusMod>();
                                    if (response != null)
                                    {
                                        foreach (string headerElement in header)
                                        {
                                            element.values = GetMod(tuples, header, response, element.values);
                                        }
                                    }
                                }
                            }
                            catch (HttpRequestException e)
                            {
                                Console.WriteLine("\nException Caught!");
                                Console.WriteLine("Message :{0} ", e.Message);
                            }
                        }
                    }
                }
                progressBar.PerformStep();
            }
            return modList;
        }

        private List<string> GetMod(List<Tuple<string, string>> tuples, List<string> header, Task<NexusMod?>? response, List<string> values)
        {
            int index = 0;
            foreach (string headerElement in header)
            {
                try
                {
                    if (string.IsNullOrEmpty(values[index]))
                    {
                        values[index] = headerElement.Contains("link")
                            ? link + response.Result.Mod_id
                            : modService.GetValue(tuples.First(t => headerElement.Contains(t.Item1)), response);
                    }
                    else if (!headerElement.Contains("current version") && httpConfiguration.UpdateNexusMods)
                    {
                        values[index] = modService.GetValue(tuples.First(t => headerElement.Contains(t.Item1)), response);
                    }
                }
                catch (Exception)
                {
                }
                index++;
            }
            return values;
        }

        public void Dispose()
        {
            client.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
