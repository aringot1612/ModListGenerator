﻿using ModListGenerator.Entities.Models;
using System.Collections.Generic;

namespace ModListGenerator.Business.Services
{
    public class CategoryService
    {
        private readonly List<Category> categories = new();
        public CategoryService()
        {
            categories = new()
            {
                new Category() { Id = 94, Name = "Alchemy" },
                new Category() { Id = 51, Name = "Animation" },
                new Category() { Id = 54, Name = "Armour" },
                new Category() { Id = 103, Name = "Armour - Shields" },
                new Category() { Id = 110, Name = "Audio - Music, SFX, Voice" },
                new Category() { Id = 26, Name = "Body, Face, and Hair" },
                new Category() { Id = 95, Name = "Bug Fixes" },
                new Category() { Id = 22, Name = "Buildings" },
                new Category() { Id = 109, Name = "Character Presets" },
                new Category() { Id = 40, Name = "Cheats and God items" },
                new Category() { Id = 53, Name = "Cities, Towns, Villages, and Hamlets" },
                new Category() { Id = 60, Name = "Clothing and Accessories" },
                new Category() { Id = 92, Name = "Collectables, Treasure Hunts, and Puzzles" },
                new Category() { Id = 77, Name = "Combat" },
                new Category() { Id = 100, Name = "Crafting" },
                new Category() { Id = 83, Name = "Creatures and Mounts" },
                new Category() { Id = 88, Name = "Dungeons" },
                new Category() { Id = 74, Name = "Environmental" },
                new Category() { Id = 96, Name = "Followers & Companions" },
                new Category() { Id = 65, Name = "Followers & Companions - Creatures" },
                new Category() { Id = 24, Name = "Gameplay" },
                new Category() { Id = 25, Name = "Guilds/Factions" },
                new Category() { Id = 78, Name = "Immersion" },
                new Category() { Id = 27, Name = "Items and Objects - Player" },
                new Category() { Id = 85, Name = "Items and Objects - World" },
                new Category() { Id = 89, Name = "Locations - New" },
                new Category() { Id = 90, Name = "Locations - Vanilla" },
                new Category() { Id = 93, Name = "Magic - Gameplay" },
                new Category() { Id = 75, Name = "Magic - Spells & Enchantments" },
                new Category() { Id = 28, Name = "Miscellaneous" },
                new Category() { Id = 82, Name = "Modders Resources and Tutorials" },
                new Category() { Id = 29, Name = "Models and Textures" },
                new Category() { Id = 33, Name = "NPC" },
                new Category() { Id = 79, Name = "Overhauls" },
                new Category() { Id = 84, Name = "Patches" },
                new Category() { Id = 67, Name = "Player homes" },
                new Category() { Id = 97, Name = "Presets - ENB and ReShade" },
                new Category() { Id = 35, Name = "Quests and Adventures" },
                new Category() { Id = 34, Name = "Races, Classes, and Birthsigns" },
                new Category() { Id = 43, Name = "Save Games" },
                new Category() { Id = 104, Name = "Shouts" },
                new Category() { Id = 73, Name = "Skills and Leveling" },
                new Category() { Id = 76, Name = "Stealth" },
                new Category() { Id = 42, Name = "User Interface" },
                new Category() { Id = 39, Name = "Utilities" },
                new Category() { Id = 62, Name = "Visuals and Graphics" },
                new Category() { Id = 108, Name = "VR" },
                new Category() { Id = 55, Name = "Weapons" },
                new Category() { Id = 36, Name = "Weapons and Armour" }
            };
        }
        public List<Category> GetCategories() { return categories; }
    }
}