﻿using ModListGenerator.Entities.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ModListGenerator.Business.Services
{
    public class ModService
    {
        private readonly CategoryService categoryService;
        private readonly List<Category> categories;

        public ModService()
        {
            categoryService = new CategoryService();
            categories = categoryService.GetCategories();
        }

        public static List<string> GetHeader(ModList modList)
        {
            if (modList != null && modList.header != null)
            {
                List<string> header = new();
                foreach (string element in modList.header)
                {
                    header.Add(element.ToLower());
                }
                return header;
            }
            return new List<string>();
        }

        public static List<Tuple<string, string>> GetTuples()
        {
            List<Tuple<string, string>>? strings = new()
            {
                Tuple.Create("id", "Mod_id"),
                Tuple.Create("category", "Category_id"),
                Tuple.Create("name", "Name"),
                Tuple.Create("author", "Author"),
                Tuple.Create("description", "Description"),
                Tuple.Create("summary", "Summary"),
                Tuple.Create("current version", "Version"),
                Tuple.Create("last version", "Version"),
                Tuple.Create("endorsement count", "Endorsement_count"),
                Tuple.Create("created time", "Created_time"),
                Tuple.Create("updated time", "Updated_time"),
                Tuple.Create("picture url", "Picture_url"),
                Tuple.Create("contains adult content", "Contains_adult_content")
            };
            return strings;
        }

        public string? GetValue(Tuple<string, string> tuple, Task<NexusMod?>? response)
        {
            if (response != null && categories != null)
            {
                if (!tuple.Item1.Contains("category"))
                {
                    return GetPropValue(response.Result, tuple.Item2);
                }
                else if (int.TryParse(GetPropValue(response.Result, tuple.Item2), out int numValue))
                {
                    return categories.Find(e => e.Id == numValue)?.Name;
                }
            }
            return "";
        }

        private static string GetPropValue(object src, string propName)
        {
            try
            {
                if (src != null)
                {
                    if (src.GetType() != null)
                    {
                        if (src.GetType().GetProperty(propName) != null)
                        {
                            if (src.GetType().GetProperty(propName).GetValue(src, null) != null)
                            {
                                return src.GetType().GetProperty(propName).GetValue(src, null).ToString();
                            }
                        }
                    }
                }
                return "";
            }
            catch (NullReferenceException)
            {
                return "";
            }
        }

        public static bool CheckModList(List<string> values, List<string> header, List<Tuple<string, string>> tuples)
        {
            for (int i = 0; i < header.Count; i++)
            {
                for (int j = 0; j < tuples.Count; j++)
                {
                    if (header[i].Contains(tuples[j].Item1))
                    {
                        if (string.IsNullOrEmpty(values[i]))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }
}
