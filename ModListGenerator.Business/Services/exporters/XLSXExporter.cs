﻿using ClosedXML.Excel;
using ModListGenerator.Entities.Models;

namespace ModListGenerator.Business.Services.exporters
{
    public class XLSXExporter : ExportService
    {
        private readonly XLWorkbook xlsxWriter;
        private readonly IXLWorksheet worksheet;
        public XLSXExporter(string exportPath) : base(exportPath)
        {
            xlsxWriter = new();
            worksheet = xlsxWriter.Worksheets.Add("Mods");
        }

        public override void Export(ModList mods)
        {
            int currentRow = 1;
            for (int i = 0; i < mods.header.Count; i++)
            {
                worksheet.Cell(currentRow, i + 1).Value = mods.header[i];
            }

            foreach (ModValues mod in mods.content)
            {
                currentRow++;
                for (int i = 0; i < mod.values.Count; i++)
                {
                    worksheet.Cell(currentRow, i + 1).Style.NumberFormat.Format = "@";
                    worksheet.Cell(currentRow, i + 1).Value = mod.values[i];
                }
            }
            xlsxWriter.SaveAs(exportPath);
        }
    }
}
