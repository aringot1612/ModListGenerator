﻿using ModListGenerator.Core.Interfaces;
using ModListGenerator.Entities.Models;

namespace ModListGenerator.Business.Services.exporters
{
    public abstract class ExportService : IExportService
    {
        protected string exportPath = "";

        public ExportService(string exportPath)
        {
            this.exportPath = exportPath;
        }

        public abstract void Export(ModList mods);
    }
}