﻿using ModListGenerator.Entities.Models;
using System.IO;
using System.Text;

namespace ModListGenerator.Business.Services.exporters
{
    public class CSVExporter : ExportService
    {
        private readonly char delimiter;

        public CSVExporter(string exportPath) : base(exportPath)
        {
            delimiter = ';';
        }

        public override void Export(ModList mods)
        {
            using StreamWriter? sw = new(exportPath, false, Encoding.UTF8);
            sw.WriteLine(string.Join(delimiter, mods.header));
            if (mods.content != null)
            {
                foreach (ModValues element in mods.content)
                {
                    sw.WriteLine(string.Join(delimiter, element.values));
                }
            }
        }
    }
}
